@extends('layouts.public')

@section('content')
    {!! Form::open(['route' => 'register']) !!}
    <h2>Pendaftaran Pengguna Baharu</h2>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <b>{{ __('auth.register_form.box1-title') }}</b><br>
            <i class="fa fa-info-circle text-warning"></i>
            <small>{{__('auth.register_form.box1-subtitle')}}</small>
        </div>
        <div class="col-sm-9">
            <div class="row">
                @component('components.input', [
                    'type' => 'select',
                    'name' => 'user_title_id',
                    'col_class' => 'col-md-4',
                    'is_required' => true,
                    'i18n' => 'users.field.user_title_id',
                    'value' => '',
                    'dataset' => \App\Repositories\Xref\UserTitleRepository::getAll()->pluck('name','id')->prepend('-- Sila Pilih --','')
                ])
                @endcomponent

                @component('components.input', [
                    'type' => 'text',
                    'name' => 'name',
                    'col_class' => 'col-md-8',
                    'is_required' => true,
                    'i18n' => 'users.field.name',
                ])
                @endcomponent

                @component('components.input', [
                    'type' => 'select',
                    'name' => 'gender_id',
                    'is_required' => true,
                    'i18n' => 'users.field.gender_id',
                    'value' => '',
                    'dataset' => \App\Repositories\Xref\GenderRepository::getAll()->pluck('name','id')->prepend('-- Sila Pilih --','')
                ])
                @endcomponent
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <b>{{ __('auth.register_form.box2-title') }}</b><sup class="text-required"><i
                    class="fa fa-asterisk"></i></sup><br>
            <i class="fa fa-info-circle text-warning"></i>
            <small>{{__('auth.register_form.box2-subtitle')}}</small>
        </div>
        <div class="col-sm-9">
            <div class="row">
                @component('components.input', [
                    'name' => 'nric',
                    'i18n' => 'people.field.nric',
                    'placeholder' => 'e.g. 631217149011',
                ])
                @endcomponent

                @component('components.input', [
                    'name' => 'old_nric',
                    'i18n' => 'people.field.old_nric',
                    'placeholder' => 'e.g. 151231',
                ])
                @endcomponent

                @component('components.input', [
                    'name' => 'passport',
                    'i18n' => 'people.field.passport',
                    'placeholder' => 'e.g. A512312',
                ])
                @endcomponent

                @component('components.input', [
                    'name' => 'army_police_number',
                    'i18n' => 'people.field.army_police_number',
                    'placeholder' => 'e.g. 631217149011',
                ])
                @endcomponent
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <b>{{ __('auth.register_form.box3-title') }}</b><br>
            <i class="fa fa-info-circle text-warning"></i>
            <small>{{__('auth.register_form.box3-subtitle')}}</small>
        </div>
        <div class="col-sm-9">
            <div class="row">
                @component('components.input', [
                    'type' => 'email',
                    'name' => 'email',
                    'i18n' => 'users.field.email_register',
                    'is_required' => true,
                ])
                @endcomponent

                @component('components.input', [
                    'name' => 'telephone_number',
                    'i18n' => 'people.field.telephone_number',
                    'is_required' => true,
                ])
                @endcomponent

                <div class="form-group col-md-12 {{ $errors->has('address_1') ? 'has-error' : '' }}">
                    {!! Form::label('address', __('people.field.addresses')) !!}
                    <sup class="text-danger"><i class="fa fa-asterisk"></i></sup>
                    {!! Form::text('address_1', null, ['class' => 'form-control m-b']) !!}
                    {!! Form::text('address_2', null, ['class' => 'form-control m-b']) !!}
                    {!! Form::text('address_3', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('address_1'))
                        <span class="help-block"><strong>{{ $errors->first('address_1') }}</strong></span>
                    @endif
                </div>

                @component('components.input', [
                    'type' => 'select',
                    'name' => 'country_id',
                    'is_required' => true,
                    'i18n' => 'people.field.country_id',
                    'value' => '132',
                    'input_class' => 'chosen-select',
                    'dataset' => \App\Repositories\Xref\CountryRepository::getList(),
                ])
                @endcomponent

                @component('components.input', [
                    'type' => 'select',
                    'name' => 'state_id',
                    'i18n' => 'people.field.state_id',
                    'input_class' => 'chosen-select',
                    'value' => null,
                    'dataset' => \App\Repositories\Xref\StateRepository::getList2()
                ])
                @endcomponent

                @component('components.input', [
                    'type' => 'select',
                    'name' => 'district_id',
                    'i18n' => 'people.field.district_id',
                    'input_class' => 'chosen-select',
                    'value' => null,
                    'dataset' => \App\Repositories\Xref\DistrictRepository::getList2()
                ])
                @endcomponent

                @component('components.input', [
                    'type' => 'select',
                    'name' => 'subdistrict_id',
                    'i18n' => 'people.field.subdistrict_id',
                    'input_class' => 'chosen-select',
                    'value' => null,
                    'dataset' => \App\Repositories\Xref\SubdistrictRepository::getList2()
                ])
                @endcomponent
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <b>{{ __('auth.register_form.box5-title') }}</b>
            <sup class="text-danger"><i class="fa fa-asterisk"></i></sup><br>
            <i class="fa fa-info-circle text-warning"></i>
            <small>{!! __('auth.register_form.box5-subtitle') !!}</small>
            <br><br>
            <a href="javascript:void(0)"
               class="btn btn-info"
               onclick="refreshCaptcha()">
                <i class="fa fa-refresh"></i> Refresh
            </a>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="form-group col-md-4 {{ $errors->has('captcha') ? 'has-error' : '' }}">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <div class="refresh-captcha">
                                {!! Captcha::img('flat') !!}
                            </div>
                            <br><br>
                            {!! Form::text('captcha', null, ['class' => 'form-control required', 'required' => 'required']) !!}
                        </div>
                        @if ($errors->has('captcha'))
                            <div class="col-12">
                                <span class="help-block"><strong>{{ $errors->first('captcha') }}</strong></span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                @component('components.input', [
                    'type' => 'checkbox',
                    'name' => 'is_agree',
                    'i18n' => 'users.field.is_agree',
                    'value' => 1,
                    'col_class' => 'col-sm-12',
                    'input_class' => 'abc-checkbox-primary',
                ])
                @endcomponent
            </div>
        </div>
    </div>
    <hr>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" id="submit_btn" class="btn btn-primary" disabled="">
                {{ __('auth.register') }}
            </button>
            <a href="/" class="btn btn-default">
                {{ __('base.cancel') }}
            </a>
        </div>
    </div>
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script>
        var data

        function refreshCaptcha() {
            $.ajax({
                url: '/captcha/api/flat',
                type: 'get',
                dataType: 'html',
                success: function (json) {
                    data = JSON.parse(json)
                    $('.refresh-captcha').find('img').attr('src', data.img)
                },
                error: function (data) {
                    alert('Try Again.')
                }
            })
        }

        $(function () {
            $('#country_id').on('change', function () {
                var state_id = 1
                console.log('tukar')
                switch ($('#country_id').val()) {
                    case '132': //malaysia
                        state_id = 1
                        $('#state_id-div').show()
                        $('#district_id-div').show()
                        $('#subdistrict_id-div').show()
                        break
                    default: // other
                        $('#state_id-div').hide()
                        $('#district_id-div').hide()
                        $('#subdistrict_id-div').hide()
                        state_id = 0
                        break
                }

                $.ajax({
                    type: 'GET',
                    url: '/api/state/list?is_malaysia=' + state_id,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data.data)
                        $('select[name="state_id"]').empty()
                        $.each(data.data, function (key, value) {
                            $('select[name="state_id"]').append('<option value="' + key + '">' + value + '</option>')
                        })
                    },
                    complete: function (data) {
                        $('#state_id').trigger('chosen:updated')
                        $('#state_id').trigger('change')
                    }
                })
            })

            $('#state_id').on('change', function () {
                console.log('tukar state_id')

                $.ajax({
                    type: 'GET',
                    url: '/api/district/list?state_id=' + $('#state_id').val(),
                    dataType: 'json',
                    success: function (data) {
                        console.log(data.data)
                        $('select[name="district_id"]').empty()
                        $.each(data.data, function (key, value) {
                            $('select[name="district_id"]').append('<option value="' + key + '" ' + (key === '' ? 'selected' : '') + ' >' + value + '</option>')
                        })
                    },
                    complete: function (data) {
                        $('#district_id').trigger('chosen:updated')
                        $('#district_id').trigger('change')
                    }
                })
            })

            $('#district_id').on('change', function () {
                console.log('tukar district_id')

                $.ajax({
                    type: 'GET',
                    url: '/api/subdistrict/list?district_id=' + $('#district_id').val(),
                    dataType: 'json',
                    success: function (data) {
                        console.log(data.data)
                        $('select[name="subdistrict_id"]').empty()
                        $.each(data.data, function (key, value) {
                            $('select[name="subdistrict_id"]').append('<option value="' + key + '" ' + (key === '' ? 'selected' : '') + '>' + value + '</option>')
                        })
                    },
                    complete: function (data) {
                        $('#subdistrict_id').trigger('chosen:updated')
                    }
                })
            })

            $('#is_agree').on('change', function () {
                let stat = $('#is_agree').prop('checked')
                $('#submit_btn').prop('disabled', function (i, v) {
                    console.log(v, stat)
                    return !v
                })
            })

            $('.chosen-select').chosen({ width: '100%' })
        })
    </script>
@endsection
