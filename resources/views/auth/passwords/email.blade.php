@extends('layouts.public')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->has('email'))
        <div class="alert alert-danger" role="alert">
            {{ $errors->first('email') }}
        </div>
    @endif
    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <h2>Tukar Kata Laluan</h2>
        <div class="form-group required">
            <input type="email"
                   id="loginform-username"
                   class="form-control required {{ $errors->has('email') ? ' has-error' : '' }}"
                   name="email"
                   placeholder="Masukkan alamat email"
                   aria-required="true">
        </div>

        <div class="form-group row mb">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary block full-width m-b">Tukar Kata Laluan</button>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6">
                <a type="button" href="{{route('landing')}}" class="btn btn-default m-b">Batal</a>
            </div>
        </div>
    </form>
@endsection
