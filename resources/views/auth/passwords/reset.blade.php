@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-sm-3">
            <b>Set Semula Kata Laluan</b><br>
            <small>Pastikan kata laluan anda memenuhi keperluan <b>minimum</b> sistem ini.</small>
            <ul>
                <li>8 karakter</li>
                <li>1 huruf (e.g. abcDEF)</li>
                <li>1 nombor (e.g. 123890)</li>
                <li>1 karakter khas (e.g. !@#$)</li>
            </ul>
        </div>
        <div class="col-md-9">
            <form method="POST" action="{{ route('password.update') }}" aria-label="{{ __('Reset Password') }}">
                <input type="hidden" name="token" value="{{ $token }}">

                @csrf

                <div class="row">
                    @component('components.input', [
                        'type' => 'email',
                        'name' => 'email',
                        'i18n' => '',
                        'placeholder' => 'Sila masukkan emel',
                        'is_required' => true,
                        'value' => $email,
                        'readonly' => 'readonly',
                        'col_class' => 'col-md-12',
                    ])
                    @endcomponent

                    @component('components.input', [
                        'type' => 'password',
                        'name' => 'password',
                        'placeholder' => 'Sila masukkan katalaluan baharu',
                        'i18n' => 'users.field.password',
                        'is_required' => true,
                        'col_class' => 'col-md-12',
                    ])
                    @endcomponent

                    @component('components.input', [
                        'type' => 'password',
                        'name' => 'password_confirmation',
                        'placeholder' => 'Sila sahkan katalaluan',
                        'i18n' => 'users.field.password_confirmation',
                        'is_required' => true,
                        'col_class' => 'col-md-12',
                    ])
                    @endcomponent

                    <div class="col" style="display:none" id="password-notice">
                        <div class="alert alert-danger">
                            Sila pastikan kata laluan anda adalah sama.
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary block full-width m-b">Tukar kata laluan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#password_confirmation').on('keyup',function(){
            comparePassword('password', 'password_confirmation')
        })
    </script>
@endsection
