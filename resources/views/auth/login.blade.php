@extends('layouts.public')

@section('content')
    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf
        <h2>Log Masuk</h2>
        <hr>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if ($errors->has('email'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('email') }}
            </div>
        @endif

        <div class="form-group required">
            <input type="email" id="loginform-username" class="form-control " name="email"
                   placeholder="Masukkan alamat email" aria-required="true">
        </div>

        <div class="form-group required">
            <input type="password" id="loginform-password" class="form-control"
                   name="password" placeholder="Kata laluan" aria-required="true">
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary block full-width m-b">{{ __('auth.login') }}</button>
            </div>
        </div>
    </form>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <a class="btn btn-danger btn-block" href="{{ url('password/reset') }}">
                <small><i class="fa fa-envelope"></i> {{__('auth.forget_password')}}</small>
            </a>
        </div>
{{--        @if(config('app.type') == 'PUBLIC')--}}
            <div class="col-md-6">
                <a class="btn btn-success btn-block" href="{{ route('register') }}">
                    <small><i class="fa fa-user"></i> {{__('auth.register')}}</small>
                </a>
            </div>
{{--        @endif--}}
    </div>
@endsection
