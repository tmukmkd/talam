@extends('layouts.public')

@section('content')
    <div class="container">
        <div class="row justify-content-center m-b-lg">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Anda perlu sahkan email anda') }}</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success m-t-md" role="alert">
                                {{ __('E-mel pengesahan akaun telah dihantar kepada e-mel anda') }}
                            </div>
                        @endif

                        {{ __('Sebelum anda meneruskan aktiviti, mohon untuk sahkan akaun anda berdasarkan emel yang telah dihantar.') }}
                        {{ __('Jika anda tidak menerima emel pengesahan') }}<br>
                            <a class="btn btn-primary m-t-md" href="{{ route('verification.resend') }}">{{ __('tekan sini untuk menghantar emel baharu') }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
