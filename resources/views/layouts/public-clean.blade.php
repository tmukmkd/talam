<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="black-bg">
<div class="col m-t-xl">
    <div class="row animated fadeInDown" style="justify-content: center">
        <div class="col-xs-12">
            <img src="{{asset('img/cms-logo.png')}}" class="logo" alt="" width="300px">
        </div>
    </div>
</div>
@yield('content')
<!-- Scripts -->
<script src="{{ mix('/js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

@section('scripts')
@show

</body>
</html>
