<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="public skin1 {{ config('app.type')  == 'INTERNAL' ? 'internal' : 'test' }}">
<div class="col m-t-xl">
    <div class="row animated fadeInDown" style="justify-content: center">
        <div class="col-xs-12">
            <img src="{{asset('img/cms-logo.png')}}" class="logo" alt="" width="300px">
        </div>
    </div>
</div>
<div class="login-box col-md-8 col-md-push-2 col-sm-8 col-sm-push-2">
    <div class="row animated fadeInDown text-center loginscreen"></div>
    <div class="row">
        <div class="col">
            <div class="{{Request::is(['login', 'password/reset', 'password/email', '/']) ? 'middle-box' : ''}}">
                <div>
                    <div class="m-b-xl">
                    </div>
                    @yield('content')
                </div>
            </div>
        </div>
{{--        @if(Request::is('login') || Request::is('/'))--}}
{{--            <div class="skip-box text-left col">--}}
{{--                <div class="middle-box">--}}
{{--                    <h2>Pengumuman</h2>--}}
{{--                    @foreach($announcements as $announcement)--}}
{{--                        <div class="m-b-lg">--}}
{{--                            <h3>{{$announcement->title}}</h3>--}}
{{--                            <span>{{$announcement->subject}}</span>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}
    </div>
    <div class="row text-center loginscreen animated fadeInDown text-white">
        <div class="col-md-12">
            <p class="m-t">
                <strong>{{ __('base.copyright') }}.</strong> &copy; {{date('Y')}} Suruhanjaya Pencegahan Rasuah Malaysia
            </p>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('/js/manifest.js') }}" type="text/javascript"></script>
    <script src="{{ mix('/js/vendor.js') }}" type="text/javascript"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

@section('scripts')
@show

</body>
</html>
