<div class="footer">
	<div class="pull-right">
		{{ date('d-m-Y') }}
	</div>
	<div>
		 <strong>{{ __('base.copyright') }}</strong> {{ __('base.owner_long_name') }} &copy; {{date('Y')}}
	</div>
</div>