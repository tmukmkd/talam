<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="" src="{{asset('img/cms-logo-small.png')}}" height="72px"/>
                    </span>
                </div>
                <div class="logo-element">
                    <img alt="image" class="" width="50px" src="{{asset('img/logo.png')}}" />
                </div>
            </li>
            @include('layouts.menu')
        </ul>
    </div>
</nav>

