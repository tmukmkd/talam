<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-inline " href="#"><i class="fa fa-bars"></i> </a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-white text-muted">
                <span class="welcome-message">
                    Login terakhir - {{ Session::get('last_login') }} | <b>{{ auth()->user()->name }}</b>
                </span>
                <a href="{{route('users.profile.edit')}}"></a>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <img src="{{ auth()->user()->latestProfileFavicon ?? asset('img/default-avatar.jpg') }}"
                         class="img-circle" width="45px" alt="">
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a class="dropdown-item" href="{{route('users.profile.edit')}}">Profil</a></li>
                    <li><a class="dropdown-item"
                           href="{{route('users.profile-password.edit')}}">Tukar Kata Laluan</a></li>
                    <li class="dropdown-divider"></li>
                    <li>
                        @impersonating
                            <a href="#"
                               onclick="event.preventDefault(); document.getElementById('impersonate-leave-form').submit();">
                                <i class="fa fa-sign-out"></i> Henti Menyamar
                            </a>
                            <form id="impersonate-leave-form" action="{{ route('admin.users.impersonateLeave') }}"
                                  method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a href="#"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> <span class="">Log keluar</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            @endImpersonating
                    </li>
                </ul>
            </span>
        </li>
        <li>

        </li>
    </ul>
</nav>
