<div class="row wrapper border-bottom white-bg page-heading breadcrumb-container">
    <div class="col-lg-9">
        <h2>@yield('page_title')</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item {{request()->segment(count(request()->segments())) == 'intra' ? 'active' : ''}}">
                <span href="/">Home</span>
            </li>
            @if(request()->segment(count(request()->segments())) != 'intra')
                <li class="breadcrumb-item active">
                    <span>@yield('page_module')</span>
                </li>
            @endif
        </ol>
    </div>
    <div class="col-lg-3">
    </div>
</div>
