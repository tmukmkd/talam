<section id="footer">
    <div class="container">
        <div class="row m-b-lg m-t-xl">
            <div class="col-sm-5 col-md-3 col-lg-6">
                <address>
                    <h3>Suruhanjaya Pencegahan Rasuah Malaysia (SPRM)</h3>
                    Ibu Pejabat SPRM,<br>
                    No.2, Lebuh Wawasan,Presint 7, <br>
                    62250 Putrajaya. <br><br>
                    <abbr title="Phone"><i class="fa fa-phone"></i></abbr> : +603 8870 0688 / +603 8870 0688 <br>
                    <abbr title="Phone"><i class="fa fa-phone"></i></abbr> : +603 8870 0934 (SMS) <br>
                    <abbr title="Phone"><i class="fa fa-phone"></i></abbr> : 1-800-88-6000 (Hotline) <br>
                    <abbr title="Fax"><i class="fa fa-fax"></i></abbr> : +603 8870 0934 <br>
                    <abbr title="Mail"><i class="fa fa-inbox"></i></abbr> : info@sprm.gov.my
                </address>
            </div>
            <div class="col-sm-5 col-lg-4">
                <div class="row">
                    <div class="col-md-4">
                        <h5>Tentang CMS</h5>
                        <a href="#">Misi CMS</a><br>
                        <a href="#">SPRM</a>
                    </div>
                    <div class="col-md-4">
                        <h5>Bantuan</h5>
                        <a href="#">FAQ</a><br>
                        <a href="#">Pusat Bantuan</a><br>
                        <a href="#">Terma & Syarat</a><br>
                        <a href="#">Polisi Privasi</a>
                    </div>
                    <div class="col-md-4">
                        <h5>Hubungi</h5>
                        <a href="#">Email</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p class="text-secondary"><strong>Hakcipta &copy; 2010
                        - {{ date('Y') }}</strong><br/>Suruhanjaya Pencegahan Rasuah Malaysia</p>
            </div>
        </div>
    </div>
</section>