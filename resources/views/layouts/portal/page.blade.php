<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body style="background-color: white" id="page-top" class="landing-page no-skin-config">
@include('layouts.portal.navbar')
<section id="content" class="container post-breadcrumb">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <ol class="breadcrumb">
            <li>
                <a href="/portal">Home</a>
            </li>
            <li class="active">
                <strong>@yield('post_title')</strong>
            </li>
        </ol>
    </div>
</div>
</section>
<section id="content" class="container post">
    <div class="row">
        <div class="col-sm-12">
            @yield('content')
        </div>
    </div>
</section>
@include('layouts.portal.contact')
<!-- Scripts -->
<script src="{{ mix('/js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

@section('scripts')
@show

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

</body>
</html>
