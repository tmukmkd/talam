<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>
<body style="background-color: white">
<div id="wrapper" class="public">
    <div id="" class="dashbard-1">
        <div class="row border-bottom">
            @include('layouts.portal.navbar')
        </div>
        <div class="row">
            <div class="col-lg-push-2 col-lg-8">
                @yield('content')
            </div>
            <div class="col-lg-push-2 col-lg-8 text-center m-b-lg">
                <strong>{{ __('base.copyright') }}</strong> {{__('base.ministry_long_name')}} &copy; 2010
                - {{date('Y')}}
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ mix('/js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

@section('scripts')
@show

<script>
  function changeLanguage(i18n) {
    $.get("{{route('i18n')}}?i18n=" + i18n).then(
      function (data) {
        // console.log(data)
        window.location.reload(true)
      },
      function (err) {
        console.error(err)
      }
    )
  }
</script>

</body>
</html>
