<div class="table-responsive">
    <table class="table" id="projectTypes-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Parent Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($projectTypes as $projectType)
            <tr>
                <td>{{ $projectType->name }}</td>
            <td>{{ $projectType->parent_id }}</td>
                <td>
                    {!! Form::open(['route' => ['z.projectTypes.destroy', $projectType->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('z.projectTypes.show', [$projectType->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('z.projectTypes.edit', [$projectType->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
