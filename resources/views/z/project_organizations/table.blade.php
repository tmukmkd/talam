<div class="table-responsive">
    <table class="table" id="projectOrganizations-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Parent Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($projectOrganizations as $projectOrganization)
            <tr>
                <td>{{ $projectOrganization->name }}</td>
            <td>{{ $projectOrganization->parent_id }}</td>
                <td>
                    {!! Form::open(['route' => ['z.projectOrganizations.destroy', $projectOrganization->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('z.projectOrganizations.show', [$projectOrganization->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('z.projectOrganizations.edit', [$projectOrganization->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
