@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Project Organization
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'z.projectOrganizations.store']) !!}

                        @include('z.project_organizations.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
