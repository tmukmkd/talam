<div class="table-responsive">
    <table class="table" id="projectMilestoneTypes-table">
        <thead>
            <tr>
                <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($projectMilestoneTypes as $projectMilestoneType)
            <tr>
                <td>{{ $projectMilestoneType->name }}</td>
                <td>
                    {!! Form::open(['route' => ['z.projectMilestoneTypes.destroy', $projectMilestoneType->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('z.projectMilestoneTypes.show', [$projectMilestoneType->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('z.projectMilestoneTypes.edit', [$projectMilestoneType->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
