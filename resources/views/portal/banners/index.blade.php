@extends('layouts.app')
@section('page_title')
{{ __(strtolower('portal.banners.index')) }}
@endsection
@section('page_module')
{{ __(strtolower('portal.banners.model')) }}
@endsection
@section('content')
@section('content')
    <section class="content-header">
        <h1 class="pull-right">
        @if(isset($auth) && $auth->user->can('admin create '.strtolower('Banners')))
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('portal.banners.create') !!}">Add New</a>
        @endif
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('portal.banners.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

