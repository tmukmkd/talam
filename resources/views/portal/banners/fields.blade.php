
<div class="row">
    <div class="col-sm-3">
        <b>{{ __(strtolower('portal.banners.model')) }} Information</b><br>
        <i class="fa fa-info-circle text-warning"></i>
        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatibus, deserunt est debitis. Unde
            earum consectetur, voluptatem quo minus facere vitae, dolor nostrum tenetur nemo voluptatibus! Provident
            nihil, sit harum?
        </small>
    </div>
    <div class="col-sm-9">
        <div class="row">
            <!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtitle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Button Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_button', 'Is Button:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_button', false) !!}
        {!! Form::checkbox('is_button', '1', null) !!} 1
    </label>
</div>

<!-- Button Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('button_title', 'Button Title:') !!}
    {!! Form::text('button_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Button Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('button_link', 'Button Link:') !!}
    {!! Form::text('button_link', null, ['class' => 'form-control']) !!}
</div>
        </div>
    </div>
</div>
