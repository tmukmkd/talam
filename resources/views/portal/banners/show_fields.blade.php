<!-- Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $banner->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group col-md-6">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $banner->title !!}</p>
</div>

<!-- Subtitle Field -->
<div class="form-group col-md-6">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    <p>{!! $banner->subtitle !!}</p>
</div>

<!-- Is Button Field -->
<div class="form-group col-md-6">
    {!! Form::label('is_button', 'Is Button:') !!}
    <p>{!! $banner->is_button !!}</p>
</div>

<!-- Button Title Field -->
<div class="form-group col-md-6">
    {!! Form::label('button_title', 'Button Title:') !!}
    <p>{!! $banner->button_title !!}</p>
</div>

<!-- Button Link Field -->
<div class="form-group col-md-6">
    {!! Form::label('button_link', 'Button Link:') !!}
    <p>{!! $banner->button_link !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group col-md-6">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $banner->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $banner->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $banner->updated_at !!}</p>
</div>

