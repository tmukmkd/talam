<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        {{--@foreach($banners as $key => $banner)--}}
        {{--<div class="item {{ $key == 0 ? 'active' : '' }}">--}}
        {{--<div class="container">--}}
        {{--<div class="carousel-caption">--}}
        {{--<h1>{!! nl2br($banner->title) !!}</h1>--}}
        {{--<p>{{$banner->subtitle}}</p>--}}
        {{--<p>--}}
        {{--@if($banner->is_button)--}}
        {{--<a class="btn btn-lg btn-primary gradient" href="{{$banner->button_link}}"--}}
        {{--role="button">{{$banner->button_title}}</a>--}}
        {{--@endif--}}
        {{--</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<!-- Set background for slide in css -->--}}
        {{--<div class="header-back"--}}
        {{--style="background: url('{{$banner->latestBanner}}'); background-size: cover; background-position: center"></div>--}}
        {{--</div>--}}
        {{--@endforeach--}}
        <div class="item active">
            <!-- Set background for slide in css -->
            <div class="header-back"
                 style="background: url('{{asset('img/macc-bg.jpg')}}'); background-size: cover; background-position: top"></div>
        </div>
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>