<div class="table-responsive">
    <table class="table" id="teams-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>User Id</th>
        <th>Organization Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($teams as $team)
            <tr>
                <td>{{ $team->project_id }}</td>
            <td>{{ $team->user_id }}</td>
            <td>{{ $team->organization_id }}</td>
                <td>
                    {!! Form::open(['route' => ['project.teams.destroy', $team->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.teams.show', [$team->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.teams.edit', [$team->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
