<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('project_id', 'Project Id:') !!}
    {!! Form::number('project_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Open Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('open_date', 'Open Date:') !!}
    {!! Form::date('open_date', null, ['class' => 'form-control','id'=>'open_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#open_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Close Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('close_date', 'Close Date:') !!}
    {!! Form::date('close_date', null, ['class' => 'form-control','id'=>'close_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#close_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('project.tasks.index') }}" class="btn btn-default">Cancel</a>
</div>
