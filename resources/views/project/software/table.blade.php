<div class="table-responsive">
    <table class="table" id="software-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>Type Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Expired Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($software as $software)
            <tr>
                <td>{{ $software->project_id }}</td>
            <td>{{ $software->type_id }}</td>
            <td>{{ $software->name }}</td>
            <td>{{ $software->description }}</td>
            <td>{{ $software->expired_date }}</td>
                <td>
                    {!! Form::open(['route' => ['project.software.destroy', $software->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.software.show', [$software->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.software.edit', [$software->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
