@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Software
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($software, ['route' => ['project.software.update', $software->id], 'method' => 'patch']) !!}

                        @include('project.software.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection