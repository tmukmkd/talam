<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('project_id', 'Project Id:') !!}
    {!! Form::number('project_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Milestone Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('milestone_type_id', 'Milestone Type Id:') !!}
    {!! Form::number('milestone_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Milestone Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('milestone_date', 'Milestone Date:') !!}
    {!! Form::date('milestone_date', null, ['class' => 'form-control','id'=>'milestone_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#milestone_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('project.milestones.index') }}" class="btn btn-default">Cancel</a>
</div>
