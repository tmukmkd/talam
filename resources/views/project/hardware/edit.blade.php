@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Hardware
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($hardware, ['route' => ['project.hardware.update', $hardware->id], 'method' => 'patch']) !!}

                        @include('project.hardware.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection