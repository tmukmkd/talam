<div class="table-responsive">
    <table class="table" id="hardware-table">
        <thead>
            <tr>
                <th>Project Id</th>
        <th>Type Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Expired Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($hardware as $hardware)
            <tr>
                <td>{{ $hardware->project_id }}</td>
            <td>{{ $hardware->type_id }}</td>
            <td>{{ $hardware->name }}</td>
            <td>{{ $hardware->description }}</td>
            <td>{{ $hardware->expired_date }}</td>
                <td>
                    {!! Form::open(['route' => ['project.hardware.destroy', $hardware->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('project.hardware.show', [$hardware->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('project.hardware.edit', [$hardware->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
