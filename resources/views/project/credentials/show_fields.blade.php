<!-- Project Id Field -->
<div class="form-group">
    {!! Form::label('project_id', 'Project Id:') !!}
    <p>{{ $credential->project_id }}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{{ $credential->type_id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $credential->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $credential->description }}</p>
</div>

<!-- Host Field -->
<div class="form-group">
    {!! Form::label('host', 'Host:') !!}
    <p>{{ $credential->host }}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{{ $credential->username }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $credential->password }}</p>
</div>

<!-- Expired Date Field -->
<div class="form-group">
    {!! Form::label('expired_date', 'Expired Date:') !!}
    <p>{{ $credential->expired_date }}</p>
</div>

