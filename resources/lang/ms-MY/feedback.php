<?php

return [
    'model' => 'Feedback',
    'index' => 'Senarai Feedback',
    'create' => 'Cipta tiket baharu',
    'show' => 'Lihat',
    'edit' => 'Kemaskini',
    'update' => 'Simpan',
    'delete' => 'Hapus',

    //label
    'description' => 'Feedback',
];
