<?php

return [
    'model' => 'Tiket Dalam Proses',
    'index' => 'Senarai tiket dalam proses',
    'create' => 'Cipta tiket baharu',
    'show' => 'Lihat',
    'edit' => 'Kemaskini',
    'update' => 'Simpan',
    'delete' => 'Hapus',

    //label
    'title' => 'Tajuk',
    'description' => 'Keterangan',
    'status' => 'Status',
    'attachment' => 'Dokumen Sokongan',
    'system' => 'Sistem Berkaitan',
    'application_number' => 'Nombor Tiket',

    'note_1' => [
        'title' => 'Maklumat Tiket',
        'description' => 'Data maklumat kepada ticket.'
    ],
    'note_2' => [
        'title' => 'Dokumen Sokongan',
        'description' => 'Gambar atau dokumen sokongan.'
    ],
    'note_3' => [
        'title' => 'Nota',
        'description' => 'Log kepada nota perbincangan.'
    ],
];
