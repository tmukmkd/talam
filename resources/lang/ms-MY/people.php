<?php

return [
    'field' => [
        'user_title_id' => 'Gelaran',
        'designation' => 'Pangkat',
        'name' => 'Nama Penuh',
        'nric' => 'No. Kad Pengenalan',
        'old_nric' => 'No. Kad Pengenalan Lama',
        'passport' => 'No. Passport',
        'army_police_number' => 'No. Pengenalan Askar / Polis',
        'telephone_number' => 'No. Telefon',
        'addresses' => 'Alamat',
        'country_id' => 'Negara',
        'state_id' => 'Negeri',
        'district_id' => 'Daerah',
        'subdistrict_id' => 'Mukim',
        'postcode' => 'Poskod',
        'identity' => 'Nombor Identiti',
    ]
];
