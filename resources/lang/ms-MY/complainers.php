<?php

return [
    'model' => 'Pengadu',
    'index' => 'Senarai Pengadu',
    'show' => 'Lihat',
    'edit' => 'Kemaskini',
    'update' => 'Simpan',
    'delete' => 'Hapus',

    'name' => 'Nama',

    'inboxed' => [
        'title' => 'Inbox (Semua)',
        'index' => 'Inbox (Semua)',
    ],

    'st_raw' => [
        'title' => 'RAW',
        'index' => 'RAW',
    ],

    'st_inbox' => [
        'title' => 'Inbox',
        'index' => 'Senarai semua maklumat (inbox)',
    ],

    'st_nfa_suggestion' => [
        'title' => 'Cadang NFA',
        'index' => 'Senarai semua maklumat (cadang NFA)',
    ],

    'st_close_nfa' => [
        'title' => 'Tutup: NFA',
        'index' => 'Senarai semua maklumat (tutup: NFA)',
    ],

    'st_qaa' => [
        'title' => 'Tutup: Pertanyaan & Permohonan',
        'index' => 'Senarai semua maklumat (tutup: Pertanyaan & Permohonan)',
    ],

    'st_kiv' => [
        'title' => 'KIV',
        'index' => 'Senarai semua maklumat (KIV)',
    ],

    'st_refer' => [
        'title' => 'Rujuk Jabaan',
        'index' => 'Senarai semua maklumat (Rujuk Jabatan)',
    ],

    'st_pre_cris' => [
        'title' => 'Pre-CRIS',
        'index' => 'Pre-CRIS',
    ],

    'st_cris' => [
        'title' => 'CRIS',
        'index' => 'CRIS',
    ],

    'notification' => [
        'title' => 'Notifikasi',
        'index' => 'Senarai Notifikasi',
    ],

    //label
    'title' => 'Tajuk',
    'description' => 'Keterangan',
    'status' => 'Status',
    'attachment' => 'Dokumen Sokongan',
    'system' => 'Sistem Berkaitan',
    'application_number' => 'Nombor Tiket',
    'urgent' => 'Urgent',
    'reference_number' => 'Nombor Maklumat',
    'complainer_id' => 'Pengadu',
    'source_id' => 'Sumber',
    'status_id' => 'Status',
    'active_user_id' => 'PYD',
    'created_at' => 'Tarikh Cipta',
    'jmm' => 'JMM',
    'noti_pre_jmm' => 'Noti. Pre',
    'noti_jmm' => 'Noti. JMM',
    'noti_io' => 'Noti. IO',

    'note_1' => [
        'title' => 'Maklumat Tiket',
        'description' => 'Data maklumat kepada ticket.'
    ],
    'note_2' => [
        'title' => 'Dokumen Sokongan',
        'description' => 'Gambar atau dokumen sokongan.'
    ],
    'note_3' => [
        'title' => 'Nota',
        'description' => 'Log kepada nota perbincangan.'
    ],
];
