<?php

return [
    'model' => 'Log status tiket',
    'index' => 'Senarai log status tiket',
    'create' => 'Cipta tiket baharu',
    'show' => 'Lihat',
    'edit' => 'Kemaskini',
    'update' => 'Simpan',
    'delete' => 'Hapus',

    //label
    'title' => 'Tajuk',
    'description' => 'Keterangan',
    'status' => 'Status',
    'attachment' => 'Dokumen Sokongan',
    'system' => 'Sistem Berkaitan',
    'application_number' => 'Nombor Tiket',
];
