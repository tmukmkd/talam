<?php

return [
    'countries' => [
        'model' => 'Negara',
        'index' => 'Senarai negara',
        'create' => 'Cipta baharu'
    ],
    'states' => [
        'model' => 'Negeri',
        'index' => 'Senarai negeri',
        'create' => 'Cipta baharu'
    ],
    'districts' => [
        'model' => 'Daerah',
        'index' => 'Senarai daerah',
        'create' => 'Cipta baharu'
    ],
    'subdistricts' => [
        'model' => 'Mukim / Bandar / Pekan',
        'index' => 'Senarai mukim / bandar / pekan',
        'create' => 'Cipta baharu'
    ],
    'categories' => [
        'model' => 'Kategori',
        'index' => 'Senarai kategori',
        'create' => 'Cipta baharu'
    ],
    'sources' => [
        'model' => 'Sumber',
        'index' => 'Senarai sumber',
        'create' => 'Cipta baharu'
    ],
    'genders' => [
        'model' => 'Jantina',
        'index' => 'Senarai jantina',
        'create' => 'Cipta baharu'
    ],
    'user_titles' => [
        'model' => 'Pangkat',
        'index' => 'Senarai pangkat',
        'create' => 'Cipta baharu'
    ],
    'zones' => [
        'model' => 'Zon',
        'index' => 'Senarai zon',
        'create' => 'Cipta baharu'
    ],
    'departments' => [
        'model' => 'Bahagian',
        'index' => 'Senarai bahagian',
        'create' => 'Cipta baharu'
    ],
];
