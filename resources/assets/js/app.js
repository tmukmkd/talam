// check url that have nav-tab
var url = document.location.toString()
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[ 1 ] + '"]').tab('show')
}

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash
})

/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.9.3
 *
 */
$(document).ready(function () {

    // Fast fix bor position issue with Propper.js
    // Will be fixed in Bootstrap 4.1 - https://github.com/twbs/bootstrap/pull/24092
    Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false


    // Add body-small class if window less than 768px
    if (window.innerWidth < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    // MetisMenu
    var sideMenu = $('#side-menu').metisMenu()

    // Collapse ibox function
    $('.collapse-link').on('click', function (e) {
        e.preventDefault()
        var ibox = $(this).closest('div.ibox')
        var button = $(this).find('i')
        var content = ibox.children('.ibox-content')
        content.slideToggle(200)
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down')
        ibox.toggleClass('').toggleClass('border-bottom')
        setTimeout(function () {
            ibox.resize()
            ibox.find('[id^=map-]').resize()
        }, 50)
    })

    // Close ibox function
    $('.close-link').on('click', function (e) {
        e.preventDefault()
        var content = $(this).closest('div.ibox')
        content.remove()
    })

    // Fullscreen ibox function
    $('.fullscreen-link').on('click', function (e) {
        e.preventDefault()
        var ibox = $(this).closest('div.ibox')
        var button = $(this).find('i')
        $('body').toggleClass('fullscreen-ibox-mode')
        button.toggleClass('fa-expand').toggleClass('fa-compress')
        ibox.toggleClass('fullscreen')
        setTimeout(function () {
            $(window).trigger('resize')
        }, 100)
    })

    // Close menu in canvas mode
    $('.close-canvas-menu').on('click', function (e) {
        e.preventDefault()
        $('body').toggleClass('mini-navbar')
        SmoothlyMenu()
    })

    // Run menu of canvas
    $('body.canvas-menu .sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    })

    // Open close right sidebar
    $('.right-sidebar-toggle').on('click', function (e) {
        e.preventDefault()
        $('#right-sidebar').toggleClass('sidebar-open')
    })

    // Initialize slimscroll for right sidebar
    $('.sidebar-container').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
    })

    // Open close small chat
    $('.open-small-chat').on('click', function (e) {
        e.preventDefault()
        $(this).children().toggleClass('fa-comments').toggleClass('fa-times')
        $('.small-chat-box').toggleClass('active')
    })

    // Initialize slimscroll for small chat
    $('.small-chat-box .content').slimScroll({
        height: '234px',
        railOpacity: 0.4
    })

    // Small todo handler
    $('.check-link').on('click', function () {
        var button = $(this).find('i')
        var label = $(this).next('span')
        button.toggleClass('fa-check-square').toggleClass('fa-square-o')
        label.toggleClass('todo-completed')
        return false
    })

    // Append config box / Only for demo purpose
    // Uncomment on server mode to enable XHR calls
    //$.get("skin-config.html", function (data) {
    //    if (!$('body').hasClass('no-skin-config'))
    //        $('body').append(data);
    //});

    // Minimalize menu
    $('.navbar-minimalize').on('click', function (event) {
        event.preventDefault()
        $('body').toggleClass('mini-navbar')
        SmoothlyMenu()

    })

    // Tooltips demo
    $('.tooltip-demo').tooltip({
        selector: '[data-toggle=tooltip]',
        container: 'body'
    })


    // Move right sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top')
        } else {
            $('#right-sidebar').removeClass('sidebar-top')
        }
    })

    $('[data-toggle=popover]')
        .popover()

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })

    // $('.chosen-select').chosen({ width: '100%' })
    // $('.chosen-select').select2()
})

// Minimalize menu when screen is less than 768px
$(window).bind('resize', function () {
    if (window.innerWidth < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
})

// Fixed Sidebar
$(window).bind('load', function () {
    if ($('body').hasClass('fixed-sidebar')) {
        $('.sidebar-collapse').slimScroll({
            height: '100%',
            railOpacity: 0.9
        })
    }
})


// check if browser support HTML5 local storage
function localStorageSupport() {
    return (('localStorage' in window) && window[ 'localStorage' ] !== null)
}

// Local Storage functions
// Set proper body class and plugins based on user configuration
$(document).ready(function () {
    if (localStorageSupport()) {

        var collapse = localStorage.getItem('collapse_menu')
        var fixedsidebar = localStorage.getItem('fixedsidebar')
        var fixednavbar = localStorage.getItem('fixednavbar')
        var boxedlayout = localStorage.getItem('boxedlayout')
        var fixedfooter = localStorage.getItem('fixedfooter')

        var body = $('body')

        if (fixedsidebar == 'on') {
            body.addClass('fixed-sidebar')
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            })
        }

        if (collapse == 'on') {
            if (body.hasClass('fixed-sidebar')) {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar')
                }
            } else {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar')
                }

            }
        }

        if (fixednavbar == 'on') {
            $('.navbar-static-top').removeClass('navbar-static-top').addClass('navbar-fixed-top')
            body.addClass('fixed-nav')
        }

        if (boxedlayout == 'on') {
            body.addClass('boxed-layout')
        }

        if (fixedfooter == 'on') {
            $('.footer').addClass('fixed')
        }
    }
})

// For demo purpose - animation css script
function animationHover(element, animation) {
    element = $(element)
    element.hover(
        function () {
            element.addClass('animated ' + animation)
        },
        function () {
            //wait for animation to finish before removing classes
            window.setTimeout(function () {
                element.removeClass('animated ' + animation)
            }, 2000)
        })
}

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        console.log('1')
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide()
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400)
            }, 200)
    } else if ($('body').hasClass('fixed-sidebar')) {
        console.log('2')
        $('#side-menu').hide()
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400)
            }, 100)
    } else {
        console.log('3')
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style')
    }
}

// Dragable panels
function WinMove() {
    var element = '[class*=col]'
    var handle = '.ibox-title'
    var connect = '[class*=col]'
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8
        })
        .disableSelection()
}

// end of inspinia script

$(document).ready(function () {
    $('.date').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    })

    $('.input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    })
})

var myBlockui = function () {
    console.log('bui open')
    $.blockUI({
        message: '<div class="blockui">\n' +
            '    <div class="boxes">\n' +
            '    <div class="box">\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '    </div>\n' +
            '    <div class="box">\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '    </div>\n' +
            '    <div class="box">\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '    </div>\n' +
            '    <div class="box">\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '        <div></div>\n' +
            '    </div>\n' +
            '</div>\n' +
            '</div><h1>Dalam Proses...</h1><p>Sila tunggu sebentar.</p>',
        baseZ: 11000
    })
}

/**
 * To delete attachment
 */
function deleteAttachment(slug) {
    swal({
        title: 'Anda pasti?',
        text: 'Maklumat yang dihapuskan tidak boleh dikembalikan',
        icon: 'warning',
        buttons: [ 'Tidak', 'Ya' ]
    }).then(function (willDelete) {
        if (willDelete) {
            $('#delete-attachment-btn-' + slug).closest('form').submit()
        }
    })
}

function deleteMassAttachment(slug) {
    var selection = $('input[name="attachments[]"]:checked').map(function () {
        return this.value
    }).get()

    var data = []
    data.push({ name: 'media', value: mapSelectionList('attachments[]').join(',') })

    swal({
        title: 'Anda pasti?',
        text: 'Maklumat yang dihapuskan tidak boleh dikembalikan',
        icon: 'warning',
        buttons: [ 'Tidak', 'Ya' ],
        dangerMode: true
    }).then(function (willDelete) {
        if (willDelete) {
            $.ajax({
                url: '/complaints/complaint-document/' + slug + '/mass-destroy',
                type: 'DELETE',
                data: $.param(data),
                dataType: 'json', // lowercase is always preferered though jQuery does it, too.
                success: function (data) {
                    console.log('success to delete data', data)
                    $('input[name="attachments[]"]').prop('checked', false)
                    $('#attachment_mass').prop('checked', false)
                    location.reload()
                },
                error: function (err) {
                    console.error(err)
                }
            })
        }
    })
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if ((charCode > 31 && charCode < 48) || charCode > 57) {
        return false
    }
    return true
}

function duplicateAttachment() {
    swal({
        title: 'Ralat',
        text: 'Terdapat fail yang pernah dimuatnaik',
        icon: 'warning'
    })
}

function submitForm(id, title = 'title', text = 'text') {
    swal({
        title: title,
        text: text,
        icon: 'warning',
        buttons: [ 'Tidak', 'Ya' ]
    }).then(function (willSubmit) {
        if (willSubmit) {
            $.blockUI()
            $('#' + id).closest('form').submit()
        }
    })
}

function mapSelectionList(name = 'selectedRaw') {
    var selection = $('input[name="' + name + '"]:checked').map(function () {
        return this.value
    }).get()

    console.log('selection', selection)

    return selection
}

/**
 * Mutated file name
 * @param file_name
 * @returns {string}
 */
function getMutatedFileName(file_name) {
    var re = /(?:\.([^.]+))?$/ // to get extension
    var file_nam_without_extension = file_name.split('.').slice(0, -1).join('.')  // filename
    var file_name_extension = re.exec(file_name)[ 0 ] // .txt

    return file_nam_without_extension.replace(/ /g, '-').replace(/\./g, '-') + file_name_extension
}

/**
 * Count title lenght
 */
function countTitle() {
    var str = new String(document.getElementById('title').value)

    var key = event.keyCode || event.charCode
    if (key == 8 || key == 46) { // 8 - backspace
        if (str.length != 0) {
            document.getElementById('countTitleId').innerHTML = str.length - 1
        }
    } else {
        document.getElementById('countTitleId').innerHTML = str.length + 1
    }
}

function comparePassword(password = 'password', password_confirmation = 'password_confirmation') {
    var password_input = $('#' + password).val()
    var password_confirmation_input = $('#' + password_confirmation).val()

    if (password_input === password_confirmation_input) {
        $('#' + password + '-div')
            .addClass('has-success')
            .removeClass('has-error')
        $('#' + password_confirmation + '-div')
            .addClass('has-success')
            .removeClass('has-error')
        $('#password-notice').hide()
    } else {
        $('#' + password + '-div')
            .addClass('has-error')
            .removeClass('has-success')
        $('#' + password_confirmation + '-div')
            .addClass('has-error')
            .removeClass('has-success')
        $('#password-notice').show()
    }
}


function checkAll() {
    $('input[name="attachments[]"]').prop('checked', $('#attachment_mass').is(':checked'))
    updateSelection()
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

function searchComplainerByName() {
    $('#complainer_name').val()
}

function getPeopleDataByPeopleId(people_id) {
    $.get('/api/users/user_detail?people_id=' + people_id)
        .then(function (response) {
            // console.log(response)
            $('#complainer_name').text(response.people.name)
            $('#complainer_id').val(response.people.complainer.id)
            $('#people_id').val(people_id)
            $('#complainer_email').text(response.people.email != null ? response.people.email : '-')
            $('#complainer_phone').text(response.people.phone != null ? response.people.phone : '-')
            $('#complainer_nric').text(response.people.nric != null ? response.people.nric : '-')
            $('#people_info_data_div').show()

            // append all compliants data if exists
            var markup = ''
            $.each(response.people.complainer.complaints, function (index, datum) {
                markup += '<tr>\n' +
                    '<td><input type="checkbox"></td>\n' +
                    '<td>' + datum.reference_number + '</td>\n' +
                    '<td>' + datum.title + '</td>\n' +
                    '<td>' + datum.status_id + '</td>\n' +
                    '<td>' + datum.status_id + '</td>\n' +
                    '</tr>'
            })
            $('#complaints_table_no_data').hide()
            $('table#complaints_table tbody').append(markup)
        })
    // $.ajax({
    //     url: '/api/users/user_detail?people_id=' + people_id,
    //     type: 'GET',
    //     dataType: 'json', // lowercase is always preferered though jQuery does it, too.
    //     success: function (data) {
    //         console.log('fetch success', data)
    //
    //         return data
    //
    //
    //         // push it into main form.
    //         // var markup = '<tr>' +
    //         //     '<td>' + '' + '</td>' +
    //         //     '<td>' + datum.people.name + '</td>' +
    //         //     '<td>Identiti: ' + datum.people.nric + '</td>' +
    //         //     '<td>' +
    //         //     '<a target="_blank" href="/suspects/' + datum.id + '/edit" class="btn btn-primary btn-xs pull-right"><i class="fa fa-pencil"></i> Kemaskini maklumat OYD</a>' +
    //         //     '</td>' +
    //         //     '</tr>'
    //         // $('table#oyd_list tbody').append(markup)
    //
    //         // $('#myModal').modal('hide')
    //     }
    // })
}

// people.create
function peopleCreate() {
    $('#people_id_selection-div').hide()
    $('#people_create_div').show()
}

function peopleStore() {

}

// cancel people.create
function cancelPeopleCreate() {
    $('#people_id_selection-div').show()
    $('#people_create_div').hide()
}

// people.change
function peopleChange() {
    $('#people_info-div').hide()
    $('#people_create-div').show()
}

// cancel people.change
function cancelPeopleChange() {
    $('#people_info-div').show()
    $('#people_create-div').hide()
}

function peopleComplaintLink(slug, people_id, type = 'complainer') {
    return new Promise(function (resolve, reject) {
        console.log('peopleComplainerLink', 'enter')
        myBlockui()

        var res = new Promise(function (resolve, reject) {
            $.get('/api/complaints/' + slug + '/people/' + people_id + '/' + type)
                .then(function (response) {
                    console.log('peopleComplainerLink', response)

                    switch (type) {
                        case 'complainer':
                            getPeopleDataByPeopleId(people_id)
                            $('#people_info_data-div').show()
                            break;
                    }

                    resolve()
                }, function (err) {
                    console.error('peopleComplainerLink', err)
                    reject()
                })
        })

        res.then(function () {
            resolve()
        }, function (err) {
            reject()
        })
    })
}

function peopleFetchByPeopleId(people_id) {
    $.get('/api/users/user_detail?people_id=' + people_id)
        .then(function (response) {

        })
}

function updatePartial(no) {
    return new Promise(function (resolve, reject) {
        myBlockui()

        console.log('open')
        switch (no) {
            case 1:
                var urlpost = '{{ route(\'form1-partial1\') }}'
                break
            case 2:
                var urlpost = '{{ route(\'form1-partial2\') }}'
                break
            case 3:
                var urlpost = '{{ route(\'form1-partial3\') }}'
                break
            default:
                reject()
        }

        // replace all ' to ` to overcome WAF block rule.
        $('#claim_details').val($('#claim_details').val().replace('\'', '`'))
        var data = $('form').serialize()
        // + '&claim_case_id=' + $('#claim_case_id').val()

        var res = new Promise(function (resolve, reject) {
            $.ajax({
                url: urlpost,
                type: 'POST',
                data: data,
                datatype: 'json'
            })
                .then(function (data) {
                    console.log(data)
                }, function (xhr, ajaxOptions, thrownError) {
                    swal('{{ trans(\'swal.unexpected_error\') }}!', thrownError, 'error')
                    reject()
                }).always(function () {
                $.unblockUI()
            })
        })

        res.then(function () {
            resolve()
        }, function (err) {
            reject()
        })
    })
}
