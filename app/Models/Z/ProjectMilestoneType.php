<?php

namespace App\Models\Z;

use App\Models\Project\Milestone;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProjectMilestoneType
 * @package App\Models\Z
 * @version March 11, 2020, 3:48 am +08
 *
 * @property \Illuminate\Database\Eloquent\Collection projectMilestones
 * @property string name
 */
class ProjectMilestoneType extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
    public $table = 'z_project_milestone_types';
    public $fillable = [
        'name'
    ];
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function milestones()
    {
        return $this->hasMany(Milestone::class, 'milestone_type_id')->withTrashed();
    }

}
