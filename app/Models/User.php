<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 * @version March 11, 2020, 1:25 pm +08
 *
 * @property \Illuminate\Database\Eloquent\Collection projectTeams
 * @property \Illuminate\Database\Eloquent\Collection projectTasks
 * @property string name
 * @property string email
 * @property string password
 * @property string l10n
 * @property string timezone
 * @property string remember_token
 * @property string confirmation_code
 * @property string|\Carbon\Carbon email_verified_at
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'email',
        'password',
        'l10n',
        'timezone',
        'remember_token',
        'confirmation_code',
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'l10n' => 'string',
        'timezone' => 'string',
        'remember_token' => 'string',
        'confirmation_code' => 'string',
        'email_verified_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'l10n' => 'required',
        'timezone' => 'required'
    ];
}
