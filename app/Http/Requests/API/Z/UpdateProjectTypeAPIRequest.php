<?php

namespace App\Http\Requests\API\Z;

use App\Models\Z\ProjectType;
use InfyOm\Generator\Request\APIRequest;

class UpdateProjectTypeAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = ProjectType::$rules;
        
        return $rules;
    }
}
