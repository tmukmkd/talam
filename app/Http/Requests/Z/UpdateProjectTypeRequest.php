<?php

namespace App\Http\Requests\Z;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Z\ProjectType;

class UpdateProjectTypeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = ProjectType::$rules;
        
        return $rules;
    }
}
