<?php

namespace App\Http\Controllers\Z;

use App\Http\Requests\Z\CreateProjectTypeRequest;
use App\Http\Requests\Z\UpdateProjectTypeRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Z\ProjectType;
use Illuminate\Http\Request;
use Flash;

class ProjectTypeController extends AppBaseController
{
    /**
     * Display a listing of the ProjectType.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /** @var ProjectType $projectTypes */
        $projectTypes = ProjectType::all();

        return view('z.project_types.index')
            ->with('projectTypes', $projectTypes);
    }

    /**
     * Show the form for creating a new ProjectType.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('z.project_types.create');
    }

    /**
     * Store a newly created ProjectType in storage.
     *
     * @param CreateProjectTypeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateProjectTypeRequest $request)
    {
        $input = $request->all();

        /** @var ProjectType $projectType */
        $projectType = ProjectType::create($input);

        Flash::success('Project Type saved successfully.');

        return redirect(route('z.projectTypes.index'));
    }

    /**
     * Display the specified ProjectType.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            Flash::error('Project Type not found');

            return redirect(route('z.projectTypes.index'));
        }

        return view('z.project_types.show')->with('projectType', $projectType);
    }

    /**
     * Show the form for editing the specified ProjectType.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            Flash::error('Project Type not found');

            return redirect(route('z.projectTypes.index'));
        }

        return view('z.project_types.edit')->with('projectType', $projectType);
    }

    /**
     * Update the specified ProjectType in storage.
     *
     * @param int $id
     * @param UpdateProjectTypeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateProjectTypeRequest $request)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            Flash::error('Project Type not found');

            return redirect(route('z.projectTypes.index'));
        }

        $projectType->fill($request->all());
        $projectType->save();

        Flash::success('Project Type updated successfully.');

        return redirect(route('z.projectTypes.index'));
    }

    /**
     * Remove the specified ProjectType from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            Flash::error('Project Type not found');

            return redirect(route('z.projectTypes.index'));
        }

        $projectType->delete();

        Flash::success('Project Type deleted successfully.');

        return redirect(route('z.projectTypes.index'));
    }
}
