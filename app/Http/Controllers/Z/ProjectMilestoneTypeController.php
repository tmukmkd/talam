<?php

namespace App\Http\Controllers\Z;

use App\Http\Requests\Z\CreateProjectMilestoneTypeRequest;
use App\Http\Requests\Z\UpdateProjectMilestoneTypeRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Z\ProjectMilestoneType;
use Illuminate\Http\Request;
use Flash;

class ProjectMilestoneTypeController extends AppBaseController
{
    /**
     * Display a listing of the ProjectMilestoneType.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /** @var ProjectMilestoneType $projectMilestoneTypes */
        $projectMilestoneTypes = ProjectMilestoneType::all();

        return view('z.project_milestone_types.index')
            ->with('projectMilestoneTypes', $projectMilestoneTypes);
    }

    /**
     * Show the form for creating a new ProjectMilestoneType.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('z.project_milestone_types.create');
    }

    /**
     * Store a newly created ProjectMilestoneType in storage.
     *
     * @param CreateProjectMilestoneTypeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateProjectMilestoneTypeRequest $request)
    {
        $input = $request->all();

        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::create($input);

        Flash::success('Project Milestone Type saved successfully.');

        return redirect(route('z.projectMilestoneTypes.index'));
    }

    /**
     * Display the specified ProjectMilestoneType.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            Flash::error('Project Milestone Type not found');

            return redirect(route('z.projectMilestoneTypes.index'));
        }

        return view('z.project_milestone_types.show')->with('projectMilestoneType', $projectMilestoneType);
    }

    /**
     * Show the form for editing the specified ProjectMilestoneType.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            Flash::error('Project Milestone Type not found');

            return redirect(route('z.projectMilestoneTypes.index'));
        }

        return view('z.project_milestone_types.edit')->with('projectMilestoneType', $projectMilestoneType);
    }

    /**
     * Update the specified ProjectMilestoneType in storage.
     *
     * @param int $id
     * @param UpdateProjectMilestoneTypeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateProjectMilestoneTypeRequest $request)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            Flash::error('Project Milestone Type not found');

            return redirect(route('z.projectMilestoneTypes.index'));
        }

        $projectMilestoneType->fill($request->all());
        $projectMilestoneType->save();

        Flash::success('Project Milestone Type updated successfully.');

        return redirect(route('z.projectMilestoneTypes.index'));
    }

    /**
     * Remove the specified ProjectMilestoneType from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            Flash::error('Project Milestone Type not found');

            return redirect(route('z.projectMilestoneTypes.index'));
        }

        $projectMilestoneType->delete();

        Flash::success('Project Milestone Type deleted successfully.');

        return redirect(route('z.projectMilestoneTypes.index'));
    }
}
