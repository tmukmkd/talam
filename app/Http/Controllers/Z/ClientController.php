<?php

namespace App\Http\Controllers\Z;

use App\Http\Requests\Z\CreateClientRequest;
use App\Http\Requests\Z\UpdateClientRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Z\Client;
use Illuminate\Http\Request;
use Flash;

class ClientController extends AppBaseController
{
    /**
     * Display a listing of the Client.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /** @var Client $clients */
        $clients = Client::all();

        return view('z.clients.index')
            ->with('clients', $clients);
    }

    /**
     * Show the form for creating a new Client.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('z.clients.create');
    }

    /**
     * Store a newly created Client in storage.
     *
     * @param CreateClientRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateClientRequest $request)
    {
        $input = $request->all();

        /** @var Client $client */
        $client = Client::create($input);

        Flash::success('Client saved successfully.');

        return redirect(route('z.clients.index'));
    }

    /**
     * Display the specified Client.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('z.clients.index'));
        }

        return view('z.clients.show')->with('client', $client);
    }

    /**
     * Show the form for editing the specified Client.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('z.clients.index'));
        }

        return view('z.clients.edit')->with('client', $client);
    }

    /**
     * Update the specified Client in storage.
     *
     * @param int $id
     * @param UpdateClientRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateClientRequest $request)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('z.clients.index'));
        }

        $client->fill($request->all());
        $client->save();

        Flash::success('Client updated successfully.');

        return redirect(route('z.clients.index'));
    }

    /**
     * Remove the specified Client from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('z.clients.index'));
        }

        $client->delete();

        Flash::success('Client deleted successfully.');

        return redirect(route('z.clients.index'));
    }
}
