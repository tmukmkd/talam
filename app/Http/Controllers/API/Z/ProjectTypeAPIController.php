<?php

namespace App\Http\Controllers\API\Z;

use App\Http\Requests\API\Z\CreateProjectTypeAPIRequest;
use App\Http\Requests\API\Z\UpdateProjectTypeAPIRequest;
use App\Models\Z\ProjectType;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class ProjectTypeController
 * @package App\Http\Controllers\API\Z
 */

class ProjectTypeAPIController extends AppBaseController
{
    /**
     * Display a listing of the ProjectType.
     * GET|HEAD /projectTypes
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request)
    {
        $query = ProjectType::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $projectTypes = $query->get();

        return $this->sendResponse($projectTypes->toArray(), 'Project Types retrieved successfully');
    }

    /**
     * Store a newly created ProjectType in storage.
     * POST /projectTypes
     *
     * @param CreateProjectTypeAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(CreateProjectTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProjectType $projectType */
        $projectType = ProjectType::create($input);

        return $this->sendResponse($projectType->toArray(), 'Project Type saved successfully');
    }

    /**
     * Display the specified ProjectType.
     * GET|HEAD /projectTypes/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            return $this->sendError('Project Type not found');
        }

        return $this->sendResponse($projectType->toArray(), 'Project Type retrieved successfully');
    }

    /**
     * Update the specified ProjectType in storage.
     * PUT/PATCH /projectTypes/{id}
     *
     * @param int $id
     * @param UpdateProjectTypeAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($id, UpdateProjectTypeAPIRequest $request)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            return $this->sendError('Project Type not found');
        }

        $projectType->fill($request->all());
        $projectType->save();

        return $this->sendResponse($projectType->toArray(), 'ProjectType updated successfully');
    }

    /**
     * Remove the specified ProjectType from storage.
     * DELETE /projectTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        /** @var ProjectType $projectType */
        $projectType = ProjectType::find($id);

        if (empty($projectType)) {
            return $this->sendError('Project Type not found');
        }

        $projectType->delete();

        return $this->sendSuccess('Project Type deleted successfully');
    }
}
