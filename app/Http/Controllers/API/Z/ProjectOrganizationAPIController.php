<?php

namespace App\Http\Controllers\API\Z;

use App\Http\Requests\API\Z\CreateProjectOrganizationAPIRequest;
use App\Http\Requests\API\Z\UpdateProjectOrganizationAPIRequest;
use App\Models\Z\ProjectOrganization;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class ProjectOrganizationController
 * @package App\Http\Controllers\API\Z
 */

class ProjectOrganizationAPIController extends AppBaseController
{
    /**
     * Display a listing of the ProjectOrganization.
     * GET|HEAD /projectOrganizations
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request)
    {
        $query = ProjectOrganization::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $projectOrganizations = $query->get();

        return $this->sendResponse($projectOrganizations->toArray(), 'Project Organizations retrieved successfully');
    }

    /**
     * Store a newly created ProjectOrganization in storage.
     * POST /projectOrganizations
     *
     * @param CreateProjectOrganizationAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(CreateProjectOrganizationAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::create($input);

        return $this->sendResponse($projectOrganization->toArray(), 'Project Organization saved successfully');
    }

    /**
     * Display the specified ProjectOrganization.
     * GET|HEAD /projectOrganizations/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            return $this->sendError('Project Organization not found');
        }

        return $this->sendResponse($projectOrganization->toArray(), 'Project Organization retrieved successfully');
    }

    /**
     * Update the specified ProjectOrganization in storage.
     * PUT/PATCH /projectOrganizations/{id}
     *
     * @param int $id
     * @param UpdateProjectOrganizationAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($id, UpdateProjectOrganizationAPIRequest $request)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            return $this->sendError('Project Organization not found');
        }

        $projectOrganization->fill($request->all());
        $projectOrganization->save();

        return $this->sendResponse($projectOrganization->toArray(), 'ProjectOrganization updated successfully');
    }

    /**
     * Remove the specified ProjectOrganization from storage.
     * DELETE /projectOrganizations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        /** @var ProjectOrganization $projectOrganization */
        $projectOrganization = ProjectOrganization::find($id);

        if (empty($projectOrganization)) {
            return $this->sendError('Project Organization not found');
        }

        $projectOrganization->delete();

        return $this->sendSuccess('Project Organization deleted successfully');
    }
}
