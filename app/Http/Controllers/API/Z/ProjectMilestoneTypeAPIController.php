<?php

namespace App\Http\Controllers\API\Z;

use App\Http\Requests\API\Z\CreateProjectMilestoneTypeAPIRequest;
use App\Http\Requests\API\Z\UpdateProjectMilestoneTypeAPIRequest;
use App\Models\Z\ProjectMilestoneType;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class ProjectMilestoneTypeController
 * @package App\Http\Controllers\API\Z
 */

class ProjectMilestoneTypeAPIController extends AppBaseController
{
    /**
     * Display a listing of the ProjectMilestoneType.
     * GET|HEAD /projectMilestoneTypes
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request)
    {
        $query = ProjectMilestoneType::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $projectMilestoneTypes = $query->get();

        return $this->sendResponse($projectMilestoneTypes->toArray(), 'Project Milestone Types retrieved successfully');
    }

    /**
     * Store a newly created ProjectMilestoneType in storage.
     * POST /projectMilestoneTypes
     *
     * @param CreateProjectMilestoneTypeAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(CreateProjectMilestoneTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::create($input);

        return $this->sendResponse($projectMilestoneType->toArray(), 'Project Milestone Type saved successfully');
    }

    /**
     * Display the specified ProjectMilestoneType.
     * GET|HEAD /projectMilestoneTypes/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            return $this->sendError('Project Milestone Type not found');
        }

        return $this->sendResponse($projectMilestoneType->toArray(), 'Project Milestone Type retrieved successfully');
    }

    /**
     * Update the specified ProjectMilestoneType in storage.
     * PUT/PATCH /projectMilestoneTypes/{id}
     *
     * @param int $id
     * @param UpdateProjectMilestoneTypeAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($id, UpdateProjectMilestoneTypeAPIRequest $request)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            return $this->sendError('Project Milestone Type not found');
        }

        $projectMilestoneType->fill($request->all());
        $projectMilestoneType->save();

        return $this->sendResponse($projectMilestoneType->toArray(), 'ProjectMilestoneType updated successfully');
    }

    /**
     * Remove the specified ProjectMilestoneType from storage.
     * DELETE /projectMilestoneTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        /** @var ProjectMilestoneType $projectMilestoneType */
        $projectMilestoneType = ProjectMilestoneType::find($id);

        if (empty($projectMilestoneType)) {
            return $this->sendError('Project Milestone Type not found');
        }

        $projectMilestoneType->delete();

        return $this->sendSuccess('Project Milestone Type deleted successfully');
    }
}
