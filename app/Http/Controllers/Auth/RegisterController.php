<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\CreateUserRequest;
use App\Models\Complainer;
use App\Models\People\People;
use App\Http\Controllers\Controller;
use App\Models\User;
use Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['email'], // change username to email as backup
            'designation' => '',
            'email' => $data['email'],
            'password' => $data['password'],
            'people_id' => $data['people_id'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \App\Http\Requests\Auth\CreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function register(CreateUserRequest $request)
    {
        $data = $request->all();

        switch (config('app.type')) {
            case 'INTERNAL':
                $data['is_internal'] = 1;
                break;
            case 'PUBLIC':
            default:
                $data['is_public'] = 1;
                break;
        }

        $data['nric'] = preg_replace('/[^\d]/i', '', $data['nric']);
        $data['old_nric'] = strtoupper(preg_replace("/\W|_/", '', $data['old_nric']));
        $data['passport'] = strtoupper(preg_replace("/\W|_/", '', $data['passport']));
        $data['army_police_number'] = strtoupper(preg_replace("/\W|_/", '', $data['army_police_number']));

        // create people first
        $people = People::create($data);

        $data['people_id'] = $people->id;
        $data['password'] = 'this is not a password'; // random non hash

        //create complainer second
        Complainer::create($data);

        // create user third
        event(new Registered($user = $this->create($data)));

        switch (config('app.type')) {
            case 'INTERNAL':
                $data['is_internal'] = 1;
                break;
            case 'PUBLIC':
            default:
                $data['is_public'] = 1;
                break;
        }

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('status', 'Sila periksa email anda dalam masa beberapa minit pengesahan dan kata laluan.');
    }
}
