    <?php

/**
 * Feedback module's config file
 */
return [
    'telegram_bot' => [
        'bot_token' => env('TELEGRAM_BOT_TOKEN'),
        'chat_id' => env('TELEGRAM_CHAT_ID'),
    ]
];