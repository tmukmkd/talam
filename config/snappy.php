<?php
return array(
    'pdf' => array(
        'enabled' => true,
        'binary'  => '/usr/local/bin/wkhtmltopdf',
//        'binary' => base_path(env('SNAPPY_WKHTMLTOPDF_PATH', 'vendor/wemersonjanuario/wkhtmltopdf-windows/bin/64bit/wkhtmltopdf')),
        'timeout' => false,
        'options' => array(),
        'env' => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary' => base_path(env('SNAPPY_WKHTMLTOIMAGE_PATH', 'vendor/wemersonjanuario/wkhtmltopdf-windows/bin/64bit/wkhtmltoimage')),
        'timeout' => false,
        'options' => array(),
        'env' => array(),
    ),
);
