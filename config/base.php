<?php

return [
    'guest' => [
        'CAN_REGISTER' => env('APP_GUEST_CAN_REGISTER', false),
        'CAN_LOGIN' => env('APP_GUEST_CAN_LOGIN', false),
    ],
];
