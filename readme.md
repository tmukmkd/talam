# Talam : Project Management

## Installation

composer install to get dependencies

``` composer install  ```

npm install to get dependencies

``` npm install ```

Migration table

``` php artisan migrate ```

Seed data to table

``` php artisan seed ```

Infyom generator

``` php artisan infyom:api_scaffold NamaModelDalamSingular --fromTable --tableName=nama_table --prefix=prefixName ```

``` php artisan infyom:scaffold NamaModelDalamSingular --fromTable --tableName=nama_table --prefix=prefixName ```
