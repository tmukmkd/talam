<?php namespace Tests\APIs;

use ApiTestTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Z\ProjectOrganization;

class ProjectOrganizationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_project_organization()
    {
        $projectOrganization = factory(ProjectOrganization::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/z/project_organizations', $projectOrganization
        );

        $this->assertApiResponse($projectOrganization);
    }

    /**
     * @test
     */
    public function test_read_project_organization()
    {
        $projectOrganization = factory(ProjectOrganization::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/z/project_organizations/'.$projectOrganization->id
        );

        $this->assertApiResponse($projectOrganization->toArray());
    }

    /**
     * @test
     */
    public function test_update_project_organization()
    {
        $projectOrganization = factory(ProjectOrganization::class)->create();
        $editedProjectOrganization = factory(ProjectOrganization::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/z/project_organizations/'.$projectOrganization->id,
            $editedProjectOrganization
        );

        $this->assertApiResponse($editedProjectOrganization);
    }

    /**
     * @test
     */
    public function test_delete_project_organization()
    {
        $projectOrganization = factory(ProjectOrganization::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/z/project_organizations/'.$projectOrganization->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/z/project_organizations/'.$projectOrganization->id
        );

        $this->response->assertStatus(404);
    }
}
