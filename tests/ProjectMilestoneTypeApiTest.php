<?php namespace Tests\APIs;

use ApiTestTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Z\ProjectMilestoneType;

class ProjectMilestoneTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_project_milestone_type()
    {
        $projectMilestoneType = factory(ProjectMilestoneType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/z/project_milestone_types', $projectMilestoneType
        );

        $this->assertApiResponse($projectMilestoneType);
    }

    /**
     * @test
     */
    public function test_read_project_milestone_type()
    {
        $projectMilestoneType = factory(ProjectMilestoneType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/z/project_milestone_types/'.$projectMilestoneType->id
        );

        $this->assertApiResponse($projectMilestoneType->toArray());
    }

    /**
     * @test
     */
    public function test_update_project_milestone_type()
    {
        $projectMilestoneType = factory(ProjectMilestoneType::class)->create();
        $editedProjectMilestoneType = factory(ProjectMilestoneType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/z/project_milestone_types/'.$projectMilestoneType->id,
            $editedProjectMilestoneType
        );

        $this->assertApiResponse($editedProjectMilestoneType);
    }

    /**
     * @test
     */
    public function test_delete_project_milestone_type()
    {
        $projectMilestoneType = factory(ProjectMilestoneType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/z/project_milestone_types/'.$projectMilestoneType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/z/project_milestone_types/'.$projectMilestoneType->id
        );

        $this->response->assertStatus(404);
    }
}
