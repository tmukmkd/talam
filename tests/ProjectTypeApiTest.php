<?php namespace Tests\APIs;

use ApiTestTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Z\ProjectType;

class ProjectTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_project_type()
    {
        $projectType = factory(ProjectType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/z/project_types', $projectType
        );

        $this->assertApiResponse($projectType);
    }

    /**
     * @test
     */
    public function test_read_project_type()
    {
        $projectType = factory(ProjectType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/z/project_types/'.$projectType->id
        );

        $this->assertApiResponse($projectType->toArray());
    }

    /**
     * @test
     */
    public function test_update_project_type()
    {
        $projectType = factory(ProjectType::class)->create();
        $editedProjectType = factory(ProjectType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/z/project_types/'.$projectType->id,
            $editedProjectType
        );

        $this->assertApiResponse($editedProjectType);
    }

    /**
     * @test
     */
    public function test_delete_project_type()
    {
        $projectType = factory(ProjectType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/z/project_types/'.$projectType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/z/project_types/'.$projectType->id
        );

        $this->response->assertStatus(404);
    }
}
