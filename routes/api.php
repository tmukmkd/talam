<?php

Route::group(['prefix' => 'z'], function () {
    Route::resource('project_milestone_types', 'Z\ProjectMilestoneTypeAPIController');
    Route::resource('clients', 'Z\ClientAPIController');
    Route::resource('project_organizations', 'Z\ProjectOrganizationAPIController');
    Route::resource('project_types', 'Z\ProjectTypeAPIController');
});

Route::resource('users', 'UserAPIController');
